

### 1.Key learning, skills and experience acquired
- 1). Today I learnt about the java stream api, understood the specific usage of the stream api, and got to know the principles of some of the api. For individuals, I did a lot of corresponding exercises today to familiarise myself with the role of stream api.
I think it's something we need to use often in our work, so we need to practice it more.
- 2) In the afternoon, I learnt about OOP and did some exercises. I learnt how to solve problems with object oriented thinking, but I am not skilled enough and need to understand this way of thinking more deeply. 



###2.Problem / Confusing / Difficulties
When performing the OOP exercise today, there was a situation where I could not get started, and next I needed to go the extra mile to understand OOp thinking to solve the problem



###3.Other Comments / Suggestion

I need to practice more in the future