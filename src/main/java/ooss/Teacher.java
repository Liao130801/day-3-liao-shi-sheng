package ooss;

import java.util.*;
import java.util.stream.Collectors;

public class Teacher extends Person implements Observer {
    private List<Klass> klasses;
    public Teacher(int id, String name, int age) {
        super(id, name, age);
        this.klasses = new ArrayList<>();
    }

    public void assignTo(Klass klass) {
        this.klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klasses.contains(klass);
    }
    @Override
    public String introduce() {
        if (klasses.isEmpty()) {
          return "My name is " + this.name + ". I am " + this.age + " years old. I am a teacher.";
        }
        return "My name is " + this.name + ". I am " + this.age + " years old. I am a teacher. I teach Class "
                    + klasses.stream().map(Klass::getNumber).map(Object::toString).collect(Collectors.joining(", ")) + ".";
    }
    public boolean isTeaching(Student student) {
        return this.klasses.contains(student.getKlass());
    }
    public void update(Student leader) {
        System.out.println("I am " + this.name + ", teacher of Class " + leader.getKlass().getNumber() + ". I know " + leader.getName() + " become Leader.");
    }

  @Override
  public void update(Observable o, Object arg) {
    Student leader = (Student) arg;
    System.out.println("I am " + this.name + ", teacher of Class " + leader.getKlass().getNumber() + ". I know " + leader.getName() + " become Leader.");
  }
}
