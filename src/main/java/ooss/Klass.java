package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    protected int number;
    protected Student leader;
    protected List<Teacher> teachers = new ArrayList<>();
    protected List<Student> students = new ArrayList<>();
    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student leader) {
        if (leader.getKlass() != null && leader.getKlass().equals(this)) {
            this.leader = leader;
            leader.setLeader(true);
          ConcreteObservable concreteObservable=registerObserver();
          concreteObservable.setData(leader);

        } else {
            System.out.println("It is not one of us.");
        }
    }
    public ConcreteObservable registerObserver(){
      ConcreteObservable concreteObservable =new ConcreteObservable();
      for (Teacher teacher : teachers) {
        concreteObservable.addObserver(teacher);
      }
      for (Student student : students) {
        if (student != leader) {
          concreteObservable.addObserver(student);
        }
      }
      return concreteObservable;
    }

    public void addTeacher(Teacher teacher) {
        teachers.add(teacher);
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public boolean isLeader(Student student) {
        return student.isLeader();
    }
    public void attach(Teacher teacher) {
        if (!teachers.contains(teacher)) {
            teachers.add(teacher);
        }
    }
    public void attach(Student student) {
        if (!students.contains(student)) {
            students.add(student);
        }
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Klass klass = (Klass) obj;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
