package ooss;

import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

public class Student extends Person implements Observer {
    public String getName() {
        return name;
    }
    protected boolean isLeader = false;
    public Klass getKlass() {
        return klass;
    }

    protected Klass klass;

    public Student(int id, String name, int age) {
      super(id,name,age);
    }

    public void setLeader(boolean leader) {
        isLeader = leader;
    }

    public boolean isLeader() {
        return isLeader;
    }

    public String introduce() {
        if (klass == null) {
          return "My name is " + this.name + ". I am " + this.age + " years old. I am a student.";
        } else if (isLeader) {
            return "My name is " + this.name + ". I am " + this.age + " years old. I am a student. I am the leader of class " + klass.getNumber() + ".";
        } else {
            return "My name is " + this.name + ". I am " + this.age + " years old. I am a student. I am in class " + klass.getNumber() + ".";
        }
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        if (this.klass == null) {
            return false;
        } else {
            return this.klass.equals(klass);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Student student = (Student) obj;
        return id == student.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void update(Student leader) {
        System.out.println("I am " + this.name + ", student of Class " + leader.getKlass().getNumber() + ". I know " + leader.getName() + " become Leader.");
    }


  @Override
  public void update(Observable o, Object arg) {
    Student leader = (Student) arg;
    System.out.println("I am " + this.name + ", student of Class " + leader.getKlass().getNumber() + ". I know " + leader.getName() + " become Leader.");
  }
}
