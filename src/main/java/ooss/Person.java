package ooss;

import java.util.Objects;

public class Person  {
    protected int id;
    protected String name;
    protected int age;

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String introduce() {
        return "My name is " + this.name + ". I am " + this.age + " years old.";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Person person = (Person) obj;
        return id == person.id;  // 比较 id 是否相等来判断是否为同一人
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);  // 定义 hashcode 基于 id
    }
}
