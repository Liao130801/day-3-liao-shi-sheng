package ooss;

import java.util.Observable;

public class ConcreteObservable extends Observable {
    protected  Student leader;
    public void setData(Student leader){
    setChanged();
    notifyObservers(leader);
  }

}
